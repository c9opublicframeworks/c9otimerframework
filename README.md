# C9OTimer
C9OTimer framework is provided by Cloud9 Online LLC to handle different functionalites of a timer.


## Requirements
- Xcode 12.0 or later
- iOS 11.0 or later
- Swift 5.0 or later

## Installation
### Cocoapods
1. This framework is available through [CocoaPods](http://cocoapods.org). 
2. Include C9OTimer in your `Pod File`:

```
pod 'C9OTimer'
```

## Usage
### Timer
```swift
// Initialization
let timer = C9OTimer()
timer.title = "Label"
timer.seconds = 60
timer.intervals = []
timer.endingBell = C9OBell(name: .baodingBell)
timer.ambientHeartbeat = false

// To setup
C9OTimerManager.shared.setup(timer: timer) // Its required once for each timer.

// To play
C9OTimerManager.shared.start()

// To pause
C9OTimerManager.shared.pause()

// To stop
C9OTimerManager.shared.stop()

// To save timer into UserDefaults
C9OTimerManager.shared.save(timer: timer)

// To delete timer
C9OTimerManager.shared.delete(timer: timer)

// To get saved timers
C9OTimerManager.shared.getTimers()

```
### Interval
```swift
let interval = C9OInterval()
interval.seconds = 30
interval.bell = C9OBell(name: .baodingBell)
```
### Bells
```swift
C9OTimerManager.shared.getBells()
```

## Observers
To Subscribe
```swift
let observerClient = C9OTimerManager.shared.addObserver(observer: self)
```

After subscribing for observer, you can listen for timer & intervals state changes by using following delegate functions:
```swift
extension ViewController: C9OPlayerDelegate {
    
    func didUpdateTimerDuration(string: String) {
    }
    
    func didUpdateIntervalDuration(string: String) {
    }
    
    func didUpdateTimerSeekProgress(value: Double) {
    }
    
    func didUpdateIntervalSeekProgress(value: Double) {
    }
    
    func didFinishTimer() {
    }
    
}
```

To Unsubscribe
```swift
C9OTimerManager.shared.removeObserver(client: observerClient)
```

## Notes

- If background mode is not enabled in your application then timer will stop working when application will be in background state. It can be enabled by **Select project in Project Navigator** -> **Signing & Capabilities** -> **+ Capability** -> **Background Modes** -> **Audio, AirPlay, and Picture in Picture**.
- To get the most from our timer, please enable **Vibrate on Ring** and **Vibrate on Silent** in **Sounds & Haptics** within your iPhone Settings App.
