Pod::Spec.new do |spec|

  spec.name                     = "C9OTimer"
  spec.version                  = "0.0.8"
  spec.summary                  = "C9OTimer framework is provided by Cloud9 Online LLC to handle different functionalites of a timer."
  spec.description              = "C9OTimer contains multiple features including creating timers, deleting timers, playing timers, adding interval to timers, choosing bells for intervals, ambient heartbeat for timer and many more."
  spec.homepage                 = "https://gitlab.com/c9opublicframeworks/c9otimerframework"
  spec.license                  = { :type => 'MIT', :text => <<-LICENSE
                                        Copyright © 2021 Cloud9 Online LLC.
                                    LICENSE
                                  }
  spec.author                   = { "Cloud9 Online" => "cloud9online@gmail.com" }
  spec.ios.deployment_target    = "11.0"
  spec.source                   = { :git => "https://gitlab.com/c9opublicframeworks/c9otimerframework.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks      = "C9OTimer.framework"
  spec.swift_versions           = ['5.0']

  spec.dependency                 "C9OCore"

  spec.user_target_xcconfig     = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.pod_target_xcconfig      = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }

end
